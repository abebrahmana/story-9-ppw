from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from app.views import index
from app.models import TopLiked
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

# Create your tests here.

class StoryUnitTest(TestCase):
    def test_story_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story_models_exist(self):
        TopLiked.objects.create(title = "Warcraft", amount = 1)
        countcontent = TopLiked.objects.all().count()
        self.assertEqual(countcontent, 1)

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def test_story9_url(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

    def test_story9_search_then_load_and_display_result(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        input = selenium.find_element_by_id('search')
        input.send_keys("Warcraft chronicle")
        button = selenium.find_element_by_id('button')
        button.click()
        selenium.implicitly_wait(10)
        self.assertIn('Warcraft', selenium.page_source)

    def test_story9_press_like_button_then_check_top_liked_books(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        # like0 = selenium.find_element_by_id('like0')
        # like0.click()
        # like1 = selenium.find_element_by_id('like1')
        # like1.click()
        # like2 = selenium.find_element_by_id('like2')
        # like2.click()
        # like3 = selenium.find_element_by_id('like3')
        # like3.click()
        # like4 = selenium.find_element_by_id('like4')
        # like4.click()
        # selenium.implicitly_wait(10)
        leaderboard = selenium.find_element_by_id('leaderboard')
        leaderboard.click()
        selenium.implicitly_wait(10)
        # self.assertIn('1', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()
