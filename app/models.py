from django.db import models

# Create your models here.
class TopLiked(models.Model):
    title = models.CharField(max_length = 256)
    amount = models.IntegerField(default = 1)

    class Meta:
        ordering = ['-amount']

    def __str__(self):
        if self.amount == 1:
            return "%s with %d like" % (self.title, self.amount)
        else:
            return "%s with %d likes" % (self.title, self.amount)
