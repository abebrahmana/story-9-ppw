function bookSearch(){
    var search = document.getElementById('search').value;
    document.getElementById('results').innerHTML = "";
    console.log(search);

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "jsonp",

        success: function(data) {
            for (i = 0; i < data.items.length; i++){
                results.innerHTML += 
                    "<tr>" + 
                        "<td>" +
                            "<img src=" + 
                            data.items[i].volumeInfo.imageLinks.thumbnail + 
                            "></img>" +
                        "</td>" +
                        "<td class='item"+i+"' id='"+ data.items[i].volumeInfo.title + "'>" +
                            data.items[i].volumeInfo.title +
                        "</td>" +
                        "<td>" +
                            data.items[i].volumeInfo.authors[0] +
                        "</td>" +
                        "<td>" +
                            data.items[i].volumeInfo.publishedDate +
                        "</td>" +
                        "<td>" +
                            data.items[i].volumeInfo.industryIdentifiers[1].identifier +
                        "</td>" +
                        "<td>" +
                            "<button id='like" + i + "' onclick='bookLike" + i + "()' type='button' class='btn btn-light'>Like</button>" +
                        "</td>" +
                    "</tr>"
            }
        },

        type: 'GET'
    });
}

function bookLike0() {
    console.log('you pressed like');
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item0")[0].id,
        },
        type : 'POST',
        success : function(response){
            alert('Like saved');
        },
    });
}   

function bookLike1() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item1")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

function bookLike2() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item2")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

function bookLike3() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item3")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}    

function bookLike4() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item4")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

function bookLike5() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item5")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

function bookLike6() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item6")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

function bookLike7() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item7")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

function bookLike8() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item8")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');    
}   

function bookLike9() {
    $.ajax({
        url : '/',
        data : {
            title : document.getElementsByClassName("item9")[0].id,
        },
        type : 'POST',
        success : function(){
            alert('Like saved');
        },
    });
    console.log('you pressed like');
}   

// function likedBooks() {
//     show = document.getElementById('liked-books-leaderboard');
//     show.innerHTML = "<p> {{ show.data }} </p><br>";
// }

document.getElementById('button').addEventListener('click', bookSearch, false);

// document.getElementById('leaderboard').addEventListener('click', likedBooks, false);